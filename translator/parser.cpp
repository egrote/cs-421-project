#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<algorithm>
#include<cstring>
#include<stdio.h>
#include "scanner.h"
#include "parser.h"

using namespace std;

ifstream fin;
ofstream fout_trans;
string saved_lexeme, saved_E_word;
token_type saved_token;
bool token_available = false, traceon = true;
//=================================================
// File parser.cpp written by Group Number: 5b
//=================================================
// ** Be sure to put the name of the programmer above each function

//==================================================
// Utility functions by: Elijah Grote
//==================================================
void trace_function(string function)
{
	if (traceon)
		cout << "Processing " << "<" << function << ">" << endl;
}

void add_TranslationFile(string line, string IR)
{
	fout_trans << line << IR << endl;
}

// i.e. Done by: Christian Lopez
// ** Need syntaxerror1 and syntaxerror2 functions (each takes 2 args)
/**
* Called when the match gets an unexpected lexeme
* @params expected - string value that is the token that was expected to be.
* @params saved_lexeme - string value that is the incorrect token.
*/
void syntax_error1(string expected, string saved_lexeme) {
	cout << "SYNTAX ERROR: expected " << expected << " but found " << saved_lexeme << endl;
	fout_error << "SYNTAX ERROR: expected " << expected << " but found " << saved_lexeme << endl;
	exit(1);
}
/**
* Called when the default part of the switch function is called.
* @params saved_lexeme - string value that is the name of the lexeme that was enetered
* @params functionName - string value that is the name of the function that called this error function.
*/
void syntax_error2(string saved_lexeme, string functionName) {
	cout << "SYNTAX ERROR: unexpected " << saved_lexeme << " found in " << functionName << endl;
	fout_error << "SYNTAX ERROR: unexpected " << saved_lexeme << " found in " << functionName << endl;
	exit(1);

}

//** Done by: Christian Lopez
bool match(token_type expected)
{
	if (next_token() != expected)
		syntax_error1(tokenStrings[(int)expected], saved_lexeme);
	token_available = false;
	cout << "MATCHED " << tokenStrings[(int)expected] << endl;
	return true;
}

//** Done by: Christian Lopez
token_type next_token()
{
	string lexeme;
	if (!token_available)
	{
		fin >> saved_lexeme;
		scanner(saved_token, saved_lexeme);
		if (traceon)
			cout << "ACTIVE TOKEN: " << tokenStrings[(int)saved_token] << endl;
		token_available = true;
	}
	return saved_token;

}

// ** Make each non-terminal into a function here
// ** Be sure to put the corresponding grammar rule above each function
// ** Done by: Elijah Grote
//<noun> ::= WORD1 | PRONOUN
void noun()
{
	trace_function("noun");
	switch (next_token())
	{
	case WORD1:
		match(WORD1);
		break;
	case PRONOUN:
		match(PRONOUN);
		break;
	default:
		syntax_error2(saved_lexeme, "noun");
	}
} //end of noun() function

//<verb> ::= WORD2
void verb()
{
	trace_function("verb");
	match(WORD2);
	return;
}

//<be> ::= IS | WAS
void be()
{
	trace_function("be");
	switch (next_token())
	{
	case IS:
		match(IS);
		break;
	case WAS:
		match(WAS);
		break;
	default:
		syntax_error2(saved_lexeme, "be");
	}

} // End

//<tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
void tense()
{
	trace_function("tense");
	switch (next_token())
	{
	case VERBPAST:
		match(VERBPAST);
		break;
	case VERBPASTNEG:
		match(VERBPASTNEG);
		break;
	case VERB:
		match(VERB);
		break;
	case VERBNEG:
		match(VERBNEG);
		break;
	default:
		syntax_error2(saved_lexeme, "tense");
	}
}


//<end> ::= (<be> #gen# PERIOD) | (DESTINATION #gen# <s3>) | (OBJECT #gen# <s2>)
//<s2> ::= <s3> | (<noun> #getEword# DESTINATION #gen# <s3>)
//<s3> ::= <verb> #getEword# #gen#
//		<tense> #gen#
//		PERIOD
void end()
{
	trace_function("end");
	switch (next_token())
	{
	case IS:
	case WAS:
		be();
		gen(LINE_be);
		break;
	case DESTINATION:
		match(DESTINATION);
		gen(LINE_DESTINATION);
		verb();
		getEword();
		gen(LINE_verb);
		tense();
		gen(LINE_tense);
		break;
	case OBJECT:
		match(OBJECT);
		gen(LINE_OBJECT);
		if (next_token() == WORD1 || next_token() == PRONOUN)
		{
			noun();
			getEword();
			match(DESTINATION);
			gen(LINE_DESTINATION);
		}
		verb();
		getEword();
		gen(LINE_verb);
		tense();
		gen(LINE_tense);
		break;
	default:
		syntax_error2(saved_lexeme, "end");
	} // End of switch() statement

	match(PERIOD);

} // End of s2() function

//<short_middle> ::= <s0> | <s3>
//<s0> ::= <noun> #getEword# <s1>
//<s3> :: = <verb> #getEword# #gen#
//		<tense> #gen#
//		PERIOD 
void short_middle()
{
	trace_function("short_middle");
	switch (next_token())
	{
	case WORD2:
		verb();
		getEword();
		gen(LINE_verb);
		tense();
		gen(LINE_tense);
		match(PERIOD);
		break;
	case WORD1:
	case PRONOUN:
		noun();
		getEword();
		end();
		break;
	default:
		syntax_error2(saved_lexeme, "short_middle");
	}
} // End of short_middle() function 

//<begin> ::= [CONNECTOR #getEword# #gen#] 
//		<noun> #getEword# SUBJECT #gen#
//		<s0> | <s3>
void begin()
{  // handle optional CONNECTOR first
	trace_function("begin");
	if (next_token() == CONNECTOR)
	{
		match(CONNECTOR);
		getEword();
		gen(LINE_CONNECTOR);
	}
	// now handle the rest of the rule
	noun();
	getEword();
	match(SUBJECT);
	gen(LINE_SUBJECT);
	short_middle();
} // End of begin() function

void story()
{
	trace_function("story");
	while (true)
	{
		switch (next_token())
		{
		case CONNECTOR:
		case WORD1:
		case PRONOUN:
			begin();
			break;
		case EOFM:
			exit(0);
		default:
			syntax_error2(saved_lexeme, "story");
		}
	}
}

//By: Christian Lopez
void getEword()
{
	if(lexicalDictionary.find(saved_lexeme) != lexicalDictionary.end())
		saved_E_word = lexicalDictionary.find(saved_lexeme)->second;
	else saved_E_word = saved_lexeme;
}

//By: Elijah Grote
void gen(line_type linetype)
{
	switch (linetype)
	{
	case LINE_CONNECTOR:
		add_TranslationFile("CONNECTOR: ", saved_E_word);
		break;
	case LINE_SUBJECT:
		add_TranslationFile("ACTOR: ", saved_E_word);
		break;
	case LINE_be:
		add_TranslationFile("DESCRIPTION: ", saved_E_word);
		add_TranslationFile("TENSE: ", tokenStrings[saved_token] + "\n");
		break;
	case LINE_OBJECT:
		add_TranslationFile("OBJECT: ", saved_E_word);
		break;
	case LINE_DESTINATION:
		add_TranslationFile("TO: ", saved_E_word);
		break;
	case LINE_verb:
		add_TranslationFile("ACTION: ", saved_E_word);
		break;
	case LINE_tense:
		add_TranslationFile("TENSE: ", tokenStrings[saved_token] + "\n");
		break;
	}
}

// The test driver to start the parser
// Done by:  Christian Lopez
int main(int argc, char** argv)
{
	//- opens the input file
	//- calls the <story> to start parsing
	//- closes the input file 
	if (argc > 1 && string(argv[1]) == "traceoff")
		traceon = false;
	string file_name;
	remove("errors.txt");
	remove("Translated.txt");
	fout_error.open("errors.txt", std::ofstream::out | std::ofstream::app);
	fout_trans.open("Translated.txt", std::ofstream::out | std::ofstream::app);
	cout << "Please enter the file you would like to translate: ";
	cin >> file_name;
	fin.open(file_name.c_str());
	if (!fin)
		cout << "The file could not be opened." << endl;
	else
	{
		story();
		cout << endl;
	}
	fin.close();
	fout_error.close();
	return 0;
}// end
 //** should require no other input files!
