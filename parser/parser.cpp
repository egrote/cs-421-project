#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<algorithm>
#include<cstring>
#include<stdio.h>
#include "scanner.h"
#include "parser.h"

using namespace std;

ifstream fin;
//ofstream fout_error;
string saved_lexeme;
token_type saved_token;
bool token_available = false, traceon = true;
//=================================================
// File parser.cpp written by Group Number: 5b
//=================================================
// ** Be sure to put the name of the programmer above each function
// i.e. Done by: Christian Lopez
// ** Need syntaxerror1 and syntaxerror2 functions (each takes 2 args)

void trace_function(string function)
{
	if(traceon)
		cout << "Processing " <<  "<" << function << ">" << endl;
}

/**
* Called when the match gets an unexpected lexeme
* @params expected - string value that is the token that was expected to be.
* @params saved_lexeme - string value that is the incorrect token.
*/
void syntax_error1(string expected, string saved_lexeme) {
	cout << "SYNTAX ERROR: expected " << expected << " but found " << saved_lexeme << endl;
	fout_error << "SYNTAX ERROR: expected " << expected << " but found " << saved_lexeme << endl;
	exit(1);
}
/**
* Called when the default part of the switch function is called.
* @params saved_lexeme - string value that is the name of the lexeme that was enetered
* @params functionName - string value that is the name of the function that called this error function.
*/
void syntax_error2(string saved_lexeme, string functionName) {
	cout << "SYNTAX ERROR: unexpected " << saved_lexeme << " found in " << functionName << endl;
	fout_error << "SYNTAX ERROR: unexpected " << saved_lexeme << " found in " << functionName << endl;
	exit(1);

}

// ** Need the updated match and next_token (with 2 global vars)
//enum tokentype { ERROR, WORD1, WORD2, PERIOD, VERB, VERBNEG, VERBPAST, VERBPASTNEG, IS, WAS, OBJECT, SUBJECT, DESTINATION, PRONOUN, CONNECTOR, EOFM };
vector<string> tokenVector{ " ERROR"," WORD1"," WORD2"," PERIOD","VERB", " VERBNEG", " VERBPAST", " VERBPASTNEG", " IS", " WAS", " OBJECT", " SUBJECT", " DESTINATION", " PRONOUN", " CONNECTOR", " EOFM " };
bool match(token_type expected)
{
	if (next_token() != expected)
		syntax_error1(tokenVector[(int)expected], saved_lexeme);
	token_available = false;
	cout << "MATCHED " << tokenVector[(int)expected] << endl;
	return true;
}

token_type next_token()
{
	string lexeme;
	if (!token_available)
	{
		fin >> saved_lexeme;
		scanner(saved_token, saved_lexeme);
		if(traceon)
		cout << "ACTIVE TOKEN: " << tokenVector[(int)saved_token] << endl;
		token_available = true;
	}
	return saved_token;

}

// ** Make each non-terminal into a function here
// ** Be sure to put the corresponding grammar rule above each function
//10 <noun> :: = WORD1 | PRONOUN
void noun()
{
	trace_function("noun");
	switch (next_token())
	{
	case WORD1:
		match(WORD1);
		break;
	case PRONOUN:
		match(PRONOUN);
		break;
	default:
		syntax_error2(saved_lexeme, "noun");
	}
} //end of noun() function

//11 <verb> ::= WORD2
void verb()
{
	trace_function("verb");
	match(WORD2);
	return;
}

//12 <be> :: = IS | WAS
void be()
{
	trace_function("be");
	switch (next_token())
	{
	case IS:
		match(IS);
		break;
	case WAS:
		match(WAS);
		break;
	default:
		syntax_error2(saved_lexeme, "be");
	}

} // End

//13 <tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
void tense()
{
	trace_function("tense");
	switch (next_token())
	{
	case VERBPAST:
		match(VERBPAST);
		break;
	case VERBPASTNEG:
		match(VERBPASTNEG);
		break;
	case VERB:
		match(VERB);
		break;
	case VERBNEG:
		match(VERBNEG);
		break;
	default:
		syntax_error2(saved_lexeme, "tense");
	}
}

//9 <end> :: = <be> | DESTINATION | OBJECT[<noun> DESTINATION] <verb><tense> PERIOD
void end()
{
	trace_function("end");
	switch (next_token())
	{
	case IS:
	case WAS:
		be();
		break;
	case DESTINATION:
		match(DESTINATION);
		verb();
		tense();
		break;
	case OBJECT:
		match(OBJECT);
		if (next_token() == WORD1 || next_token() == PRONOUN)
		{
			noun();
			match(DESTINATION);
		}
		verb();
		tense();
		break;
	default:
		syntax_error2(saved_lexeme, "end");
	} // End of switch() statement
	
	match(PERIOD);

} // End of s2() function

//8 <s1> :: = <verb> <tense> PERIOD | <noun> <s2>
void short_middle()
{
	trace_function("short_middle");
	switch (next_token())
	{
	case WORD2:
		verb();
		tense();
		match(PERIOD);
		break;
	case WORD1:
	case PRONOUN:
		noun();
		end();
		break;
	default:
		syntax_error2(saved_lexeme, "short_middle");
	}
} // End of s1() function 

//7 <begin> :: = [CONNECTOR] <noun> SUBJECT <short_middle>
void begin()
{  // handle optional CONNECTOR first
	trace_function("begin");
	if (next_token() == CONNECTOR)
		match(CONNECTOR);
	// now handle the rest of the rule
	noun();
	match(SUBJECT);
	short_middle();
} // End of s() function

//Done by: Elijah Grote
void story()
{
	trace_function("story");
	while (true)
	{
		switch (next_token())
		{
		case CONNECTOR:
		case WORD1:
		case PRONOUN:
			begin();
			break;
		case EOFM:
			exit(0);
		default:
			syntax_error2(saved_lexeme, "story");
		}
	}
}

// The test driver to start the parser
// Done by:  Elijah Grote
int main(int argc, char** argv)
{
	//- opens the input file
	//- calls the <story> to start parsing
	//- closes the input file 
	if(argc > 1 && string(argv[1]) == "traceoff")
		traceon = false;
	string file_name;
	remove("errors.txt");
	fout_error.open("errors.txt", std::ofstream::out | std::ofstream::app);
	cout << "Please enter the file you would like to translate: ";
	cin >> file_name;
	fin.open(file_name.c_str());
	if (!fin)
		cout << "The file could not be opened." << endl;
	else
	{
		story();
		cout << endl;
	}
	fin.close();
	fout_error.close();

	return 0;
}// end
 //** should require no other input files!
