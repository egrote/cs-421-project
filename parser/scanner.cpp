#include<iostream>
#include<fstream>
#include<string>
#include<map>
#include<cstring>
#include "scanner.h"

using namespace std;

const char 
vowels[6]{ 'a','e','i','o','u','\0' },
cons1[9]{ 'b','g','h','k','m','n','p','r', '\0' },
cons2[6]{ 'd','j','w','y','z','\0' },
word2[3]{ 'I','E', '\0' };

///List for Pronouns for convenience
const string PronounList[5]{ "watashi", "anata", "kare", "kanojo", "sore" };
///List for Connectors for convenience
const string ConnectorList[4]{ "mata", "soshite", "shikashi", "dakara" };
//=====================================================
// File scanner.cpp written by: Group Number: 5B 
//=====================================================

///
///Universal Character checking///
///Params: char c -character to check
///        const char* letterType -array to check
/// ** Done by: Elijah Grote
bool CheckCharacter(char c, const char letterType[])
{
	int size = strlen(letterType);
	for (int i = 0; i < size; i++)
	{
		if (c == letterType[i])
			return true;
	}
	return false;
}


// ** MYTOKEN DFA to be replaced by the WORD DFA
// ** Done by: Christian Lopez
// ** RE:    (vowel | vowel n | consonant vowel | consonant vowel n |
//            consonant - pair vowel | consonant - pair vowel n) ^ +
bool mytoken(string s)
{
	string state = "q0"; //starting state
	char currentChar;
	int charpos = 0; //character index
	//cout << "Trying the WORD DFA machine..." << endl;
	while (s[charpos] != '\0') //goes through each char in string
	{
		currentChar = tolower(s[charpos]);
		//cout << "current state: " << state << endl;
		//cout << "character: " << s[charpos] << endl;
		//the following if-else statements set the states
		//according to the drawn DFA
		if (state == "q0" || state == "q0qn")
		{
			if (CheckCharacter(currentChar, vowels))
				state = "q0qn";
			else if (state == "q0qn" && currentChar == 'n')
				state = "q0";
			else if (CheckCharacter(currentChar, cons1))
				state = "q0yqc";
			else if (CheckCharacter(currentChar, cons2))
				state = "qc";
			else if (currentChar == 't')
				state = "qtsqc";
			else if (currentChar == 's')
				state = "qchqc";
			else if (currentChar == 'c')
				state = "qch";
			else
				state = "unknown";
		}
		else if (state == "q0yqc")
		{
			if (CheckCharacter(currentChar, vowels))
				state = "q0qn";
			else if (currentChar == 'y')
				state = "qc";
			else
				state = "unknown";
		}
		else if (state == "qtsqc")
		{
			if (CheckCharacter(currentChar, vowels))
				state = "q0qn";
			else if (currentChar == 's')
				state = "qc";
			else
				state = "unknown";
		}
		else if (state == "qchqc")
		{
			if (CheckCharacter(currentChar, vowels))
				state = "q0qn";
			else if (currentChar == 'h')
				state = "qc";
			else
				state = "unknown";
		}
		else if (state == "qc")
		{
			if (CheckCharacter(currentChar, vowels))
				state = "q0qn";
			else
				state = "unknown";
		}
		else if (state == "qch")
		{
			if (currentChar == 'h')
				state = "qc";
			else
				state = "unknown";
		}
		else
		{
			//cout << "I am stuck in state " << state << endl;
			return false;
		}
		charpos++;
	}//end of while
	//cout << "current state: " << state << endl;
	// where did I end up????
	if (state == "q0" || state == "q0qn")
		return true;  // end in a final state
	else
		return false;

}



// ** Update the tokentype to be WORD1, WORD2, PERIOD and ERROR.
//types of words


// ** Need the reservedwords list to be set up here
/// ** Done by: Elijah Grote
token_type CheckIfReservedWord(string w, token_type initialType)
{
	string tType = w;
	ifstream fin;
	string reservedWord = "";
	int pronounListSize = sizeof(PronounList) / sizeof(string),
		connectorListSize = sizeof(ConnectorList) / sizeof(string);

	if (w == "masu")
		return VERB;
	else if (w == "masen")
		return VERBNEG;
	else if (w == "mashita")
		return VERBPAST;
	else if (w == "masendeshita")
		return VERBPASTNEG;
	else if (w == "desu")
		return IS;
	else if (w == "deshita")
		return WAS;
	else if (w == "o")
		return OBJECT;
	else if (w == "wa")
		return SUBJECT;
	else if (w == "ni")
		return DESTINATION;
	if (w == "eofm")
		return EOFM;
	else {
		for (int i = 0; i < pronounListSize; i++)
		{
			if (w == PronounList[i])
				return PRONOUN;
		}
		for (int j = 0; j < connectorListSize; j++)
		{
			if (w == ConnectorList[j])
				return CONNECTOR;
		}
	}

	return initialType;

}
// ** Do not require any file input for these.
// ** a.out should work without any additional files.

/// ** Done by: Elijah Grote
token_type checkLastChar(string w)
{
	if (CheckCharacter(w.back(), vowels) || w.back() == 'n')
		return WORD1;
	else if (CheckCharacter(w.back(), word2))
		return WORD2;
	return ERROR;

}

///** Done by Christian Lopez
bool isPeriodToken(string s) {
	int state = 0;
	int charpos = 0;
	//while (s[charpos] != '\0') 
	{
		switch (s[charpos]) {
		case '.':
			// Going to state 1:
			state = 1;
			break;
		} // End of switch statement
	} // End of while 
	if (state != 0) {
		return true;
	}
	return false;
}


// Scanner processes only one word each time it is called
// ** Done by: Elijah Grote & Daniel Gonzalez
int scanner(token_type& the_type, string& w)
{
	// ** Grab the next word from the file
	// This goes through all machines one by one on the input string
	/*
	2. Call the token functions one after another (if-then-else)
	And generate a lexical error if both DFAs failed.
	Let the token_type be ERROR in that case.
	3. Make sure WORDs are checked against the reservedwords list
	If not reserved, token_type is WORD1 or WORD2.
	4. Return the token type & string  (pass by reference)
	*/
	// This goes through all machines one by one on the input string
	//cout << "Scanner was called..." << endl;
	fout_error.open("errors.txt", std::ofstream::app);
	if (mytoken(w))
		the_type = checkLastChar(w); //Check if word1 or word2
	else if (isPeriodToken(w)) //if not a word, check if period
		the_type = PERIOD;
	the_type = CheckIfReservedWord(w, the_type); //Check if reserved word (outside for EOF)
	if(the_type == ERROR) //none of the FAs returned TRUE
	{	cout << "Lexical Error: " << w <<" is not in the language" << endl;
		fout_error << "Lexical Error: " << w << " is not in the language" << endl;
	}
	fout_error.close();
	return 0;



}//the end



// // The test driver to call the scanner repeatedly  
// // ** Done by:  Elijah Grote & Daniel Gonzalez
//int main()
//{
//	/*
//	1. get the input file name from the user
//	2. open the input file which contains a story written in Japanese (fin.open).
//	3. call Scanner repeatedly until the EOF marker is read, and
//	each time cout the returned results
//	e.g. STRING TOKEN-TYPE
//	=====  ===========
//	watashi PRONOUN  (from the first call)
//	wa      SUBJECT  (from the second call)
//	gakkou  WORD1
//	etc.
//	*/
//	//variable declaration
//	token_type thetype;
//	string theword;
//	ifstream fin;
//	string file_name;
//
//	cout << "Please enter the file you would like to translate: ";
//	cin >> file_name;
//	fin.open(file_name.c_str());
//	if (!fin)
//		cout << "The file could not be opened." << endl;
//	else
//	{
//		while (!fin.eof())  //have not reached the end of the file
//		{
//			fin >> theword;
//			thetype = ERROR;
//			scanner(thetype, theword);  // the paramers will be set by Scanner
//			cout /*<< "Word is: "*/ << theword << " ";//endl;
//			// ** display the actual token_type instead of a number
//			cout /*<< "Type is: "*/ << tokenVector[thetype] << endl;
//			cout << endl;
//		}
//	}
//	/*hile(true)
//
//	{ } */
//	return 0;
//	// ** close the input file
//
//}// end
