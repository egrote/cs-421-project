#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<algorithm>
#include "scanner.h"

using namespace std;

enum line_type { LINE_CONNECTOR, LINE_SUBJECT, LINE_be, LINE_OBJECT, LINE_DESTINATION, LINE_verb, LINE_tense };
// ** Need the updated match and next_token (with 2 global vars)
vector<string> tokenStrings{ " ERROR"," WORD1"," WORD2"," PERIOD","VERB", " VERBNEG", " VERBPAST", " VERBPASTNEG", " IS", " WAS", " OBJECT", " SUBJECT", " DESTINATION", " PRONOUN", " CONNECTOR", " EOFM " };

void trace_function(string function);

void add_TranslationFile(string line, string IR);

void syntax_error1(string expected, string saved_lexeme);

void syntax_error2(string saved_lexeme, string functionName);

bool match(token_type expected);

token_type next_token();

void noun();

void verb();

void be();

void tense();

void end();

void short_middle();

void begin();

void story(); 

void getEword();

void gen(line_type linetype);


