//#pragma once
#ifndef SCANNER_H
#define SCANNER_H
#include<iostream>
#include<fstream>
#include<string>
#include<map>

using namespace std;

static ofstream fout_error;
// ** Need the lexicon to be set up here (to be used in Part C)
static map<string, string> lexicalDictionary = { { "watashi","I/me" },{ "anata","you" },
{ "kare", "he/him" },{ "kanojo","she/her" },{ "sore","it" },{ "mata", "Also" },
{ "soshite","Then" },{ "shikashi","However" },{ "dakara","Therefore" } };

enum token_type { ERROR, WORD1, WORD2, PERIOD, VERB, VERBNEG, VERBPAST, VERBPASTNEG, IS, WAS, OBJECT, SUBJECT, DESTINATION, PRONOUN, CONNECTOR, EOFM };

bool CheckCharacter(char c, const char letterType[]);

bool mytoken(string s);

token_type CheckIfReservedWord(string w, token_type initialType);

token_type checkLastChar(string w);

bool isPeriodToken(string s);

int scanner(token_type& the_type, string& w);

#endif
